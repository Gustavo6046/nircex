const crypto = require('crypto');
const fs = require('fs');
const util = require('util');
const path = require('path');
const Writable = require('stream').Writable;


function yearSalt(year) {
    return `NIRCEX${year}PASSWORD`;
}

function readPassword(name, key) {
    let bank;

    if (fs.existsSync('passbank.json'))
        bank = JSON.parse(fs.readFileSync('passbank.json', 'utf-8'))
    
    else
        bank = {};

    if (!bank[name])
        throw new Error(`No such password field could be found: ${util.inspect(name)}!`);
    
    let pass = bank[name];
    let decipher = crypto.createDecipher('aes-256-cbc', key + yearSalt(pass.year));

    let dec = decipher.update(pass.data, 'base64', 'utf-8');
    return dec + decipher.final('utf-8');
}

function storePassword(name, password, key) {
    let bank;

    if (fs.existsSync('passbank.json'))
        bank = JSON.parse(fs.readFileSync('passbank.json', 'utf-8'))

    else
        bank = {};

    let year = (new Date()).getFullYear().toString();

    let ciph = crypto.createCipher('aes-256-cbc', key + yearSalt(year));
    let data = ciph.update(password, 'utf-8', 'base64')
    data += ciph.final('base64');

    bank[name] = {
        year: year,
        data: data
    }

    fs.writeFileSync('passbank.json', JSON.stringify(bank), 'utf-8');
}

module.exports = {
    storePassword: storePassword,
    readPassword: readPassword,
    yearSalt: yearSalt
};

if (require.main === module) {
    const readline = require('readline');

    const stdmut = new Writable({
        write: (chunk, encoding, callback) => {
            if (stdmut.open)
                process.stdout.write(chunk, encoding);
                
            callback();
        }
    });

    stdmut.write('test\n')

    stdmut.open = true;

    const rl = readline.createInterface({
        input: process.stdin,
        output: stdmut,
        terminal: true
    });

    console.log('---------');
    console.log('Crypto Password System (CPS)');
    console.log(' v0.1.0');
    console.log();
    console.log('by Gustavo R. Rehermann.');
    console.log('---------');
    console.log();
    
    console.log('Welcome to the CPS! This utility program will');
    console.log('walk you through the process of storing a password');
    console.log('for your NIRCEX bot with safety, using cryptography.');
    console.log();
    console.log('## Instructions!');
    console.log('PLEASE read the below carefully, in order to be able to');
    console.log('utilize of this program with success.');
    console.log();
    console.log('First off, you will have to provide a private key in');
    console.log('order to save the password. This private key is not');
    console.log('required to be the same for all the passwords.');
    console.log();
    console.log('* Tip: Since we are storing important data that can');
    console.log('not be forgotten or otherwise made unusable, use a');
    console.log('simple and easily remembered private key. If you somehow');
    console.log('forget it, you can just encrypt your password again with');
    console.log('another name!');
    console.log();
    console.log('Then, you will have to insert a password name. It is used');
    console.log('to identify the password, to store multiple passwords; who');
    console.log('knows how many passwords and accounts your bot will use across');
    console.log('all the IRC networks. It might also be a nice sort of mnemonic');
    console.log('to be associated with the private key, so you can remember it');
    console.log('with more ease. It is the string you will insert in the');
    console.log('`password` field of your configuration file (likely nircex.json).');
    console.log();
    console.log('Finally, you will insert your actual password, with the which');
    console.log('your bot will be able to authenticate with NickServ.');
    console.log();
    console.log('But enough text and boring nonsense (that nobody will read anyway),');
    console.log('let\'s begin now!');
    console.log();
    console.log('## Fetching Required Information');

    function ask(question, callback) {
        stdmut.open = true;
        console.log();

        rl.question(question, function(answer) {
            stdmut.open = true;
            callback(answer);
        });
        
        stdmut.open = false;
    }

    ask(' *  Please insert your private key: ', function(pk) {
        console.log();
        rl.question(' *  Now type your password\'s identification name: ', function(id) {
            ask(' *  Finally, type the password itself: ', function(pass) {
                console.log();
                console.log('------');
                console.log();
                console.log('## Encrypting...');
                console.log();

                storePassword(id, pass, pk);

                console.log();
                console.log('# Done!');
                console.log('Enjoy your newly saved password.');
                console.log(` * Password ID: ${id}`);
                console.log(`Do not forget your private key!`);

                if (pk.length > 3)
                    console.log(`(It has ${pk.length} characters, begins with ${pk[0]} and ends with ${pk.slice(-1)}.)`);
                    
                console.log();
                process.exit(0);
            });
        });
    });
}