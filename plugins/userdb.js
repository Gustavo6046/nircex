// User Database plugin.
const sqlite3 = require('sqlite3');
const path = require('path');

module.exports = (nircex, bot) => {
    const db = new sqlite3.Database(path.join(bot.dbFolder, 'users.db'));
    
    db.run('CREATE TABLE Users (nickname TEXT, ident TEXT, hostname TEXT, realname TEXT, channels TEXT, server TEXT)', (error) => {
        if (error != null && error.message.indexOf('already exists') == -1) throw error;
    });

    function registerUser(conn, properties) {
        let nick = properties.nick;
        let ident = properties.ident;
        let hostname = properties.hostname;
        let realname = properties.realName;

        let channels = Object.keys(conn.channelUsers).filter((c) => conn.channelUsers[c].indexOf(nick) > -1);

        db.all('SELECT * FROM Users WHERE nickname = ?', nick, (err, rows) => {
            if (rows != null && rows.length > 0) {
                JSON.parse(rows[0].channels).forEach((c) => {
                    if (channels.indexOf(c) == -1) channels.push(c);
                });
                
                db.run('DELETE FROM Users WHERE nickname = ?', nick);
            }

            db.run('INSERT INTO Users VALUES (?, ?, ?, ?, ?, ?)', [
                nick,
                ident,
                hostname,
                realname,
                JSON.stringify(channels),
                conn.id
            ]);
        });
    }
        
    return {
        handlers: [
            {
                predicates: [nircex.predicates.irc.who],
                callback: (conn, msg) => registerUser(conn, msg.properties)
            }
        ],

        commands: [
            {
                name: "who",
                callback: (conn, msg, reply) => {
                    msg.args.forEach((ch) => {
                        conn.utility('join', ch, 1.2);
                        setTimeout(() => {
                            conn.utility('part', ch, 'whoops');
                        }, 2000);
                    });

                    reply(`Fine, sent WHO to ${msg.args.length} channels.`)
                }
            },

            {
                name: 'udbsize',
                callback: (conn, msg, reply) =>
                    db.all('SELECT * FROM Users', (err, rows) => {
                        let allHere = conn.allUsers.length;
                        let knownHere = rows.filter((r) => r.server == conn.id).length;

                        if (err != null)
                            reply(err.toString());

                        else
                            reply(`Got ${rows.length} users in the database. (of which ${knownHere} were in this network; ${Math.round(100 * knownHere / allHere) / 100} times the number of those known right now)`)
                    })
            },

            {
                name: 'udb',
                callback: (conn, msg, reply) => {
                    nick = msg.args[0];

                    if (nick == null)
                        return reply("Please supply someone's nickname, so I can find them in the database!");
                    
                    db.get('SELECT * FROM Users WHERE nickname = ? AND server = ?', [nick, conn.id], (err, row) => {
                        if (err != null)
                            reply(`(${msg.properties.sourceNick}: error fetching data!) ${err.toString()}`);

                        else if (row == null)
                            reply("No such user found (by nickname)!");

                        else
                            reply(`User found! Last known associated information here: [ Nickname: ${row.nickname} | Ident: ${row.ident} | Hostname: ${row.hostname} | Real name: '${row.realname}' | Known related channels: ${JSON.parse(row.channels).join(', ')} ]`)
                    })
                }
            }
        ]
    };
};