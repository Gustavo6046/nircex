// A plugin reloader utility, and other core things...
const fs = require('fs');

module.exports = (nircex, bot) => ({
    commands: [
        {
            name: 'reload',
            callback: (conn, msg) => {
                let config = JSON.parse(fs.readFileSync('nircex.json'));

                config.plugins.forEach((pl) => {
                    console.log('RELOADING PLUGIN: ' + pl);
                    bot.loadPlugin(pl);
                });

                conn.utility('message', msg.properties.target, 'Reloaded with success!');
            }
        },
        
        {
            name: 'setperm',
            level: 2500,
            callback: (conn, msg) => {
                if (msg.args.length < 2) {
                    conn.utility('message', msg.properties.target, 'Syntax: setperm <percentage> <target hostmask>');
                    return;
                }

                let perc = +msg.args[0];
                let mask = msg.args.slice(1).join(' ');

                bot.perms.add(mask, perc);
                conn.utility('message', msg.properties.target, `/!\\ Added ${perc}% trust level to regex mask:  ${mask}`);
            }
        },

        {
            name: 'raw',
            level: 5000,
            callback: (conn, msg) => {
                conn.send(msg.args.join(' '));
            }
        },

        {
            name: 'list',
            callback: (conn, msg, reply) => {
                reply(`Known commands: ${bot.commandNames.join(', ')}`);
            }
        }
    ]
});