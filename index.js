#!node
/* eslint-disable no-control-regex */
/* eslint-disable no-useless-escape */
const discord = require('discord.js');
const util = require('util');
const Writable = require('stream').Writable;
const cps = require('./cps.js');
const EventEmitter = require('events');
const path = require('path');
const fs = require('fs');
const net = require('net');
const tls = require('tls');

const beginTime = new Date();

const nircex = {
    msgTypes: {}
};

let idverse = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_';

function choice(l) {
    return l[Math.floor(Math.random() * l)];
}

function randomID(length = 50) {
    return new Array(length).fill(null).map(() => choice(idverse)).join('');
}

nircex.IntervalGate = class IntervalGate {
    constructor(out, options = {}, start = true) {
        this.out = out;
        this.interval = options.interval || 0.9;
        this.burstLimit = options.burstLimit || 3;
        this.burst = 0;
        this.chunkSize = options.chunkSize || 0;
        this.encoding = options.encoding || 'utf-8';

        let burstCooldown = options.burstCooldown || this.interval;
        setInterval(() => this.burst--, burstCooldown);

        this.queue = [];
        this.waiting = false;

        if (start)
            this.mainLoop();
    }

    write(data) {
        if (this.chunkSize == 0)
            this.queue = this.queue.concat(data.split('\n'));

        else
            this.queue = this.queue.concat(data.split(''));

        if (this.waiting)
            this.mainLoop();
    }

    mainLoop() {
        if (this.queue.length == 0) {
            this.waiting = true;
            return;
        }

        else
            this.waiting = false;

        if (this.chunkSize == 0)
            this.out.write(this.queue.shift(), this.encoding);

        else
            this.out.write(this.queue.splice(0, this.chunkSize).join(''), this.encoding);

        if (this.burst++ < this.burstLimit)
            setImmediate(() => this.mainLoop());

        else
            setInterval(() => this.mainLoop(), this.interval);
    }
};

nircex.Event = class Event {
    constructor(data) {
        this.time = new Date();
        this.data = data;
    }

    unixTime() {
        return +this.time;
    }
};

function genericFormat(formatString, values) {
    Object.keys(values).forEach((k) => formatString = formatString.replace(new RegExp(`\\{${k}\\}`, 'g'), values[k]));
    return formatString.replace(/\{\{/g, '{').replace(/\}\}/g, '}');
}

nircex.MessageType = class MessageType {
    constructor(name, expr, contentIndex = 0, properties = {}, format = '', priority = 50) {
        this.name = name;
        this.expr = expr;
        this.properties = properties;
        this.contentIndex = contentIndex;
        this.priority = priority;
        this.fmt = format;

        nircex.msgTypes[this.name] = this;
    }

    serialize(msg) {
        return genericFormat(this.fmt, msg.properties);
    }

    priority() {
        return this.priority;
    }

    predicate(event) {
        return event.data.match(this.expr) != null;
    }

    formatProperty(prop, groups) {
        if ((typeof prop) === 'number')
            prop = groups[prop] || null;

        else if ((typeof prop) === 'string')
            groups.forEach((g, i) => prop = prop.replace(new RegExp(`(?<!\\{)\\{${i}\\}(?!\\})`, 'gi'), g));

        prop = prop.replace(/\}\}/g, '}').replace(/\{\{/g, '{');
        return prop;
    }

    parse(event, connection) {
        let groups = this.expr.exec(event.data);

        if (groups == null) return null;

        let properties = {};
        let content = groups[this.contentIndex];

        Object.keys(this.properties).forEach((k) => properties[k] = this.formatProperty(this.properties[k], groups));

        return new nircex.Message(this.name, event, properties, content, connection.id);
    }
};

nircex.ircTypes = [
    new nircex.MessageType('privmsg', /^:([^\!]+)\!([^\@]+)\@(\S+) PRIVMSG (\S+) \:(.+)$/i, 5, {
        sourceNick: 1,
        sourceIdent: 2,
        sourceHost: 3,
        source: '{1}!{2}@{3}',
        target: 4,
        message: 5
    }, '{source} PRIVMSG {target} :{message}'),

    new nircex.MessageType('notice', /^:([^\!]+)\!([^\@]+)\@(\S+) NOTICE (\S+) \:(.+)$/i, 5, {
        sourceNick: 1,
        sourceIdent: 2,
        sourceHost: 3,
        source: '{1}!{2}@{3}',
        target: 4,
        message: 5
    }, '{source} NOTICE {target} :{message}'),

    new nircex.MessageType('chanList', /^:(\S+) 353 (\S+) (.) (\S+) :(.+)$/i, 5, {
        server: 1,
        _: 2,
        mode: 3,
        channel: 4,
        users: 5
    }, '{server} 353 {_} {mode} {channel} :{users}', 100),

    new nircex.MessageType('who', /^:(\S+) 352 (\S+) (\S+) (\S+) (\S+) (\S+) (\S+) (\S+) :(\S+) (.+)$/i, 5, {
        server: 1,
        _1: 2,
        channel: 3,
        ident: 4,
        hostname: 5,
        targServer: 6,
        nick: 7,
        mode: 8,
        _2: 9,
        realName: 10
    }, '{server} 352 {_1} {channel} {ident} {hostname} {targServer} {nick} {mode} :{_2} {realName}', 100),

    new nircex.MessageType('numberCode', /^:(\S+) (\d+) (\S+) \:(.+)$/, 4, {
        server: 1,
        code: 2,
        _: 3,
        message: 4,
    }, '{server} {code} {_} :{message}'),

    new nircex.MessageType('numberCode', /^:(\S+) (\d+) (\S+) \:(.+)$/, 4, {
        server: 1,
        code: 2,
        _: 3,
        message: 4,
    }, '{server} {code} {_} :{message}'),

    new nircex.MessageType('part', /^:([^\!]+)\!([^\@]+)\@(\S+) PART (#\S+) \:(.+)$/i, 5, {
        sourceNick: 1,
        sourceIdent: 2,
        sourceHost: 3,
        source: '{1}!{2}@{3}',
        channel: 4,
        reason: 5
    }, '{source} PART {channel} :{reason}'),

    new nircex.MessageType('join', /^:([^\!]+)\!([^\@]+)\@(\S+) JOIN (#\S+)/i, 0, {
        sourceNick: 1,
        sourceIdent: 2,
        sourceHost: 3,
        source: '{1}!{2}@{3}',
        channel: 4,
    }, '{source} JOIN {channel}'),

    new nircex.MessageType('quit', /^:([^\!]+)\!([^\@]+)\@(\S+) QUIT \:(.+)$/i, 4, {
        sourceNick: 1,
        sourceIdent: 2,
        sourceHost: 3,
        source: '{1}!{2}@{3}',
        reason: 4,
    }, '{source} QUIT :{reason}'),

    new nircex.MessageType('ping', /^PING :(.+)$/i, 1, {
        data: 1,
    }, 'PONG :{data}'),

    new nircex.MessageType('nick', /^:([^\!]+)\!([^\@]+)\@(\S+) NICK :(\S+)$/i, 2, {
        sourceNick: 1,
        sourceIdent: 2,
        sourceHost: 3,
        source: '{1}!{2}@{3}',
        newNick: 4,
    }, '{source} NICK :{newNick}'),
];

nircex.Message = class Message {
    constructor(type, event, properties = {}, content = null, connection = null) {
        this.type = type;
        this.event = event;
        this.properties = properties;
        this.content = content;
        this.connection = connection;
    }

    serialize() {
        return nircex.msgTypes[this.type].serialize(this);
    }

    json() {
        return JSON.stringify({
            properties: this.properties,
            content: this.content,
            time: +this.event.time,
            data: this.event.data,
            type: this.type,
            connection: this.connection
        });
    }

    static fromJson(j) {
        let msg = JSON.parse(j);
        let evt = new nircex.Event(msg.data);
        evt.time = new Date(msg.time);

        return new nircex.Message(msg.type, evt, msg.properties, msg.content, msg.connection);
    }
};

nircex.PermissionBase = class PermissionBase {
    constructor(dbFilename, defaultPerm = 100) {
        this.defaultPerm = defaultPerm;
        this.dbFilename = dbFilename;

        if (!fs.existsSync(dbFilename)) {
            this.database = [];
            this.save();
        }

        else
            this.database = JSON.parse(fs.readFileSync(dbFilename, 'utf-8'));
    }

    add(hostRegex, level) {
        this.database = this.database.concat([{ match: hostRegex, level: level }], this.database);
        this.save();
    }

    save() {
        fs.writeFileSync(this.dbFilename, JSON.stringify(this.database));
    }

    get(host) {
        let res = this.defaultPerm;

        this.database.forEach((perm) => {
            if (host.match(new RegExp(perm.match, 'i')))
                res = perm.level;
        });

        return res;
    }
};

nircex.MessageBase = class MessageBase extends EventEmitter {
    constructor(databaseFilename, parsers = []) {
        super();

        this.filename = databaseFilename;
        //this.database = new sqlite3.Database(databaseFilename);
        this.parsers = parsers;

        /*
        this.database.run('CREATE TABLE Messages (json TEXT);', (_, err) => {
            if (err != null && err.message !== 'table Messages already exists')
                throw err;
        });

        this.database.all('SELECT json FROM Messages', (err, rows) => {
            if (err != null)
                this.history = [];

            else
                this.history = rows.map((x) => nircex.Message.fromJson(x.json));
        });
        */

        this.history = [];
    }

    writeEvent(evt, conn) {
        let msg = null;
        let prio = null;

        this.parsers.forEach((p) => {
            if (p.predicate(evt)) {
                let res = p.parse(evt, conn);

                if (res != null) {
                    let pprio = p.priority;

                    if (prio == null || pprio > prio) {
                        msg = res;
                        prio = pprio;
                    }
                }
            }
        });

        if (msg != null) {
            console.log(`(${conn.id}) << ${msg.type}: ${evt.data}`);
            this.store(msg);
            this.emit('message', msg);
        }

        else
            console.log(`(${conn.id}) << unknown: ${evt.data}`);
    }

    registerType(p) {
        this.parsers.push(p);
    }

    store(msg) {
        this.history.push(msg);

        //this.database.run('INSERT INTO Messages VALUES (?)', [msg.json()]);
    }
};

nircex.Handler = class Handler {
    constructor(predicates, handleFunc, name = null) {
        this.name = name;
        this.predicates = predicates;
        this.handle = handleFunc;
    }

    _handle(connection, message) {
        if (this.predicates.every((f) => f(message))) return this.handle(connection, message) || null;
        return null;
    }
};

nircex.OneShotHandler = class OneShotHandler extends nircex.Handler {
    constructor(predicates, handleFunc, name = null) {
        super(predicates, handleFunc, name);
        this.executed = false;
    }

    _handle(conn, msg) {
        if (this.executed) return null;
        this.executed = true;

        super._handle(conn, msg);
    }
};

nircex.DiscordIRCProxy = class DiscordIRCProxy {
    constructor(token, listenPort) {
        this.proxyClient = new discord.Client();
        this.clients = [];
        this.listenServer = net.createServer((socket) => {
            let index = this.clients.length;
            let c = new nircex.DiscordIRCProxyClient(socket, this);

            socket.on('end', () => this.clients[index] = null);
            socket.on('data', (msg) => c.recv(msg));

            this.clients.push(c);
            
            if (this.ready) c.ready();
        });

        this.ready = false;

        this.listenServer.listen(listenPort, () => {
            console.log(`New Discord bot IRC proxy listening at port ${listenPort}`);
        });

        this.proxyClient.on('ready', () => {
            this.ready = true;
            this.clients.forEach((c) => {
                if (c != null) c.ready();
            });
        });

        this.proxyClient.on('message', (msg) => {
            console.log(`$$ (Discord) || <${msg.author.username}> ${msg.content}`);

            this.clients.forEach((c) => {
                if (c != null && c.identified && msg.author.id !== this.proxyClient.user.id) {
                    c.sendRaw(`:${msg.author.username.replace(/ /g, '-')}!${msg.author.username.replace(/ /g, '-') || 'default'}@discord PRIVMSG #${msg.channel.guild.id}-${msg.channel.name}${msg.channel.calculatedPosition} :${msg.content}`);
                }
            });
        });
        
        this.proxyClient.login(token);
    }
};

nircex.DiscordIRCProxyClient = class DiscordIRCProxyClient {
    constructor(socket, proxy) {
        this.socket = socket;
        this.proxy = proxy;
        this.inputBuffer = '';
        this.closed = false;
        
        this.identified = false;
        this.nick = null;
        this.ident = null;
        this.realName = '';

        socket.on('close', () => {
            this.closed = true;
            this.isClosed(null);
        });
    }

    isClosed(reason) {
        this.proxy.clients.forEach((c) => {
            if (c != null && !c.closed)
                c.sendRaw(`:${this.nick}!${this.ident}@irc QUIT${reason != null ? `: ${reason}` : ''}`);
        });
    }

    recv(data) {
        data = data.toString('utf-8');
        this.inputBuffer += data;
        let msgs = this.inputBuffer.split(/[\r\n]/).filter((x) => !!x);
        this.inputBuffer = this.inputBuffer.split(/[\r\n]/).slice(-1)[0];

        msgs.forEach(function(m) {
            m = m.replace(/\r$/, '');
            console.log(`$$$ (Discord) << ${m}`);
            let args = m.split(' ');

            if (!this.identified) {
                if (args[0].toUpperCase() === 'NICK') {
                    if (this.proxy.clients.some((c) => c != null && !c.closed && c.nick === args[1])) {
                        this.sendRaw(`433 :Nickname '${args[1]}' already in use!`);
                        this.socket.close();
                        return;
                    }

                    this.nick = args[1];

                    if (this.nick == null) {
                        this.nick = null; // non-strict equality
                        return;
                    }

                    if (this.ident != null) {
                        this.identified = true;
                        this.isIdentified();
                    }
                }

                else if (args[0].toUpperCase() === 'USER') {
                    if (args.length < 2) return;

                    this.ident = args[1] || 'default';

                    if (args.length > 4)
                        this.realName = args.slice(3).join(' ').replace(/^: /, '');

                    if (this.nick != null) {
                        this.identified = true;
                        this.isIdentified();
                    }
                }

                return;
            }
            
            if (args[0].toUpperCase() === 'PRIVMSG') {
                let target = args[1];
                if (target == null || target === '') return;

                let content = args.slice(2).join(' ').replace(/^\:/, '');

                let parts = target.split('-');
                let guildId = parts[0];
                let channelName = parts.slice(1).join('-');

                // form the Discord message
                let processed;
                let act = /^\u0001ACTION (.+?)\u0001$/.exec(content);

                if (act != null)
                    processed = `**\\* ${this.nick}** ${act[1]}`;

                else
                    processed = `**<${this.nick}>** ${content}`;

                if (!this.proxy.proxyClient.channels.some((c) => {
                    if (c.guild != null && c.send != null && '#' + c.guild.id === guildId && c.name.replace(/ /g, '-') + c.calculatedPosition === channelName) {
                        // eslint-disable-next-line no-control-regex
                        c.send(processed);

                        console.log(`$$$ (Discord .G ${c.guild.name} - ${channelName}) >> ${processed}`);
                        return true;
                    }

                    return false;
                })) {
                    if (this.proxyClient.clients.filter((c) => c != null).map((c) => c.nick).indexOf(target) > -1) {
                        let c = this.proxyClient.clients[this.proxyClient.clients.map((c) => c != null ? c.nick : '').indexOf(target)];
                        c.sendRaw(`:${this.nick}!${this.ident}@irc PRIVMSG ${c.nick} :${content}`);
                    }

                    else {
                        if (!this.proxy.proxyClient.users.some((u) => {
                            if (u.send != null && u.username === target) {
                                u.send(processed);

                                console.log(`$$$ (Discord .U ${u.username}) >> ${processed}`);
                                return true;
                            }

                            return false;
                        })) {
                            this.proxy.proxyClient.guilds.forEach((g) => {
                                if (g.members != null)
                                    g.members.forEach((m) => {
                                        if (m.displayName === target) {
                                            m.send(processed);

                                            console.log(`$$$ (Discord .U/M ${m.user.username} : ${m.displayName}) >> ${processed}`);
                                            return true;
                                        }
                                    });

                                return false;
                            });
                        }
                    }
                }

                else {
                    this.proxy.clients.forEach((cl) => {
                        if (cl != null && `${cl.nick}!${this.ident}` !== `${this.nick}!${this.ident}`)
                            cl.sendRaw(`:${this.nick}!${this.ident}@irc PRIVMSG ${target} :${content}`);
                    });
                }
            }

            else if (args[0].toUpperCase() === 'WHOIS') {
                let target = args[1];
                if (target === null) return;

                if (!this.proxy.clients.some((cl) => {
                    if (cl != null && cl.nick === target) {
                        this.sendRaw(`:irc ${cl.closed ? 314 : 311 /*whowas*/} ${cl.nick} ${cl.ident} irc * :${cl.realName}`);

                        if (!cl.closed)
                            this.sendRaw(`:irc 312 ${cl.nick} irc :The IRC interface to Nircex' Discord proxy.`);
                            
                        this.sendRaw(`:irc 318 ${cl.nick} :No more information available.`);
                        return true;
                    }

                    return false;
                })) {
                    this.proxy.proxyClient.guilds.forEach((guild) => {
                        guild.members.every((user) => {
                            let stati = {
                                online: 'Online',
                                offline: 'Offline',
                                idle: 'Away',
                                dnd: 'Busy'
                            };

                            if (user.displayName != null && user.displayName.replace(/ /g, '-') === target) {
                                this.sendRaw(`:irc 311 ${this.nick} ${user.displayName.replace(/ /g, '-')} ${user.displayName.replace(/ /g, '-')} discord * :${stati[user.presence.status]}${user.presence.game != null ? ` (${user.presence.game.name})` : ''}`);
                                this.sendRaw(`:irc 312 ${this.nick} ${user.displayName.replace(/ /g, '-')} discord :The Discord interface to Nircex' Discord proxy.`);
                                this.sendRaw(`:irc 318 ${this.nick} ${user.displayName.replace(/ /g, '-')} :No more information available.`);                                

                                return false;
                            }

                            return true;
                        });
                    });
                }
            }
            
            else if (args[0].toUpperCase() === 'QUIT') {
                let reason = null;
                if (args.length > 1) reason = args.slice(1).join(' ').replace(/^\:/, '');

                this.socket.destroy();
                this.closed = true;
                this.isClosed(reason);
            }
        }.bind(this));
    }

    sendRaw(data, suffix = '\r\n') {
        if (!this.closed)
            this.socket.write(data + suffix);
    }

    ircRepresentMembers(memberList) {
        let opModes = {
            normal: '',
            operator: '@',
            halfOperator: '%',
            voice: '+'
        };
        let res = [];
        let rawList = [];

        memberList.forEach((member) => {
            let mode = opModes.normal;

            if (member.permissions.has('ADMINISTRATOR'))
                mode = opModes.operator;

            else if (['KICK_MEMBERS', 'DEAFEN_MEMBERS'].some(member.permissions.has.bind(member.permissions)))
                mode = opModes.halfOperator;

            else if (member.presence.status !== 'offline')
                mode = opModes.voice;

            res.push(mode + member.displayName.replace(/ /g, '-'));
            rawList.push(member.displayName.replace(/ /g, '-'));
        });

        this.proxy.clients.forEach((c) => {
            if (c != null && c.identified && rawList.indexOf(c.nick) === -1) {
                res.push(opModes.voice + c.nick);
                rawList.push(c.nick);
            }
        });

        return res.join(' ');
    }

    ready() {
        console.log('---- New client connected.');
    }

    isIdentified() {
        this.sendRaw(`:irc 001 ${this.nick} :Welcome to the Internet Relay Network ${this.nick}!${this.ident}@irc`);
        this.sendRaw(`:irc 002 ${this.nick} :Your host is Nircex, running version ${JSON.parse(fs.readFileSync(path.join(__dirname, 'package.json'))).version}`);
        this.sendRaw(`:irc 003 ${this.nick} :This server was created ${beginTime}`);
        this.sendRaw(`:irc 004 ${this.nick} :Nircex ${JSON.parse(fs.readFileSync(path.join(__dirname, 'package.json'))).version}  `);
        
        this.proxy.proxyClient.channels.tap(function (channel) {
            if (channel.guild != null && channel.send != null) {
                this.proxy.clients.forEach((c) => {
                    if (c != null)
                        c.sendRaw(`:${this.nick}!${this.ident}@irc JOIN #${channel.guild.id}-${channel.name.replace(/ /g, '-') + channel.calculatedPosition}`);
                });

                this.sendRaw(`:irc 353 _ = #${channel.guild.id}-${channel.name.replace(/ /g, '-') + channel.calculatedPosition} :${this.ircRepresentMembers(channel.guild.members)}`);
            }
        }, this);
    }
};

nircex.Connection = class Connection extends EventEmitter {
    constructor(manager, id, host, port, configurers = {}, intervalOptions = {}, flags = []) {
        super();

        this.configurers = configurers;
        this.manager = manager;
        this.id = id;
        this.uuid = randomID(65);
        this.closed = false;
        this.buf = '';
        this._out = null;
        this.handlers = [];
        this.utils = {};
        this.flags = flags.filter((x, i) => flags.indexOf(x) == i);

        this.socket = new net.Socket();

        if (this.getFlag('TLS'))
            this.socket = new tls.TLSSocket(this.socket);

        this.prepareSocket();

        this.connProm = new Promise((resolve) => {
            this.socket.on('connect', () => {
                this._out = new nircex.IntervalGate(this.socket, intervalOptions, true);

                this.initialize();
                this.emit('connected', this.socket);

                resolve(this);
            });

            this.socket.on('error', (err) => {
                console.warn(`Error with connection to ${this.id}:`);
                this.close();

                throw err;
            });

            this.socket.connect(port, host);
        });
    }

    getFlag(flag) {
        return this.flags.indexOf(flag) > -1;
    }

    registerHandler(handler) {
        this.handlers.push(handler);
    }

    prepareSocket() {
        this.socket.on('data', (msg) => this._recv(msg));
        this.socket.on('close', () => this.close(true));
    }

    send(data) {
        if (this.closed)
            throw new Error('Attempted sending data through closed IRC connection!');

        if (Array.isArray(data))
            data.forEach((msg) => this.send(msg));

        else {
            this._out.write(data + '\r\n');
            console.log(`(${this.id})`, '>>', data);
        }
    }

    _recv(data) {
        this.buf += data;

        let sep = this.buf.split('\n').map((x) => x.replace(/\r$/, ''));
        this.buf = sep.slice(-1)[0];

        sep.slice(0, -1).forEach((data) => {
            this.manager.msgBase.writeEvent(new nircex.Event(data), this);
            this.emit('data', data);
        });
    }

    close(hereOnly, reason = null) {
        if (!hereOnly)
            this.socket.destroy();

        this.emit('close');
        this.closed = true;
        this.isClosed(reason);
    }

    initialize() {
        this.configurers.forEach((cfg) => {
            let listeners = cfg.listeners || {};
            Object.keys(listeners).forEach((k) => this.on(k, listeners[k](this)));

            cfg.handlers.forEach((h) => this.registerHandler(h));

            Object.assign(this.utils, cfg.utils || {});
        });
    }

    utility(name) {
        return this.utils[name].bind(this).apply(this, [this].concat(Array.from(arguments).slice(1)));
    }
};

nircex.predicates = {
    irc: {
        anyMessage: (msg) => msg.type == 'privmsg',
        publicMessage: (msg) => msg.type == 'privmsg' && msg.properties.target[0] == '#',
        notice: (msg) => msg.type == 'notice',
        join: (msg) => msg.type == 'join',
        chanList: (msg) => msg.type == 'chanList',
        part: (msg) => msg.type == 'part',
        quit: (msg) => msg.type == 'quit',
        ping: (msg) => msg.type == 'ping',
        nick: (msg) => msg.type == 'nick',
        who: (msg) => msg.type == 'who',
        welcome: (msg) => msg.type == 'numberCode' && ['001', '376', '422'].indexOf(msg.properties.code) > -1
    }
};

nircex.configurers = {
    irc: function configureIrc(nickname, ident, realName, channels = [], joinDelay = 8) {
        let data = {
            listeners: {
                connected: (connection) => function onConnected() {
                    connection.allUsers = [];
                    connection.channelUsers = {};

                    connection.send([`NICK ${nickname}`, `USER ${ident} * 8 :${realName}`]);
                },

                join: (conn) => function onJoin(msg) {
                    if (conn.allUsers.indexOf(msg.properties.sourceNick) == -1)
                        conn.allUsers.push(msg.properties.sourceNick);

                    conn.channelUsers[msg.channel] = (conn.channelUsers[msg.channel] || []).concat([msg.properties.sourceNick]);
                },

                clist: (conn) => function onJoin(msg) {
                    let chan = msg.properties.channel;
                    let users = msg.properties.users.split(' ').map((u) => u.replace(/^[\+\@\!]/, ''));

                    users.forEach((u) => {
                        if (conn.allUsers.indexOf(u) == -1)
                            conn.allUsers.push(u);
                    });

                    conn.channelUsers[chan] = (conn.channelUsers[chan] || []).concat(users);
                },

                part: (conn) => function onPart(msg) {
                    if (conn.allUsers.indexOf(msg.properties.sourceNick) > -1) {
                        conn.channelUsers[msg.channel].splice(conn.channelUsers[msg.channel].indexOf(msg.properties.sourceNick), 1);
                    }
                },

                quit: (conn) => function onQuit(msg) {
                    if (conn.allUsers.indexOf(msg.properties.sourceNick) > -1) {
                        conn.allUsers.splice(conn.allUsers.indexOf(msg.properties.sourceNick), 1);

                        Object.keys(conn.channelUsers).forEach((c) => {
                            if (conn.channelUsers[c].indexOf(msg.properties.sourceNick) > -1)
                                conn.channelUsers[c].splice(conn.channelUsers[c].indexOf(msg.properties.sourceNick), 1);
                        });
                    }
                },

                nick: (conn) => function onNick(msg) {
                    if (conn.allUsers.indexOf(msg.properties.sourceNick) > -1) {
                        conn.allUsers.splice(conn.allUsers.indexOf(msg.properties.sourceNick), 1);

                        Object.keys(conn.channelUsers).forEach((c) => {
                            if (conn.channelUsers[c].indexOf(msg.properties.sourceNick) > -1)
                                conn.channelUsers[c][conn.channelUsers[c].indexOf(msg.properties.sourceNick)] = msg.properties.new;
                        });
                    }

                    if (conn.allUsers.indexOf(msg.properties.new) == -1)
                        conn.allUsers.push(msg.properties.new);
                }
            },

            handlers: [
                new nircex.Handler([nircex.predicates.irc.ping], (conn, msg) => conn.send(msg.serialize())), // PINGs serialize to PONGs.

                new nircex.Handler([nircex.predicates.irc.join], (conn, msg) => conn.emit('join', msg)),
                new nircex.Handler([nircex.predicates.irc.part], (conn, msg) => conn.emit('part', msg)),
                new nircex.Handler([nircex.predicates.irc.quit], (conn, msg) => conn.emit('quit', msg)),
                new nircex.Handler([nircex.predicates.irc.nick], (conn, msg) => conn.emit('nick', msg)),
                new nircex.Handler([nircex.predicates.irc.chanList], (conn, msg) => conn.emit('clist', msg)),

                new nircex.OneShotHandler([nircex.predicates.irc.welcome], (conn) => {
                    setTimeout(() => {
                        console.log(`** Waited ${joinDelay} seconds after welcome message; joining channels ${channels.join(', ')}`);

                        channels.forEach((c) => {
                            conn.utility('join', c);
                        });
                    }, joinDelay * 1000);
                }),
            ],

            utils: {
                message: (conn, target, message) => {
                    let msgs;
                    let msgsSplit = [];

                    if (Array.isArray(message))
                        msgs = message;

                    else
                        msgs = [message];

                    msgs.forEach((m) => {
                        if (m != null)
                            msgsSplit = msgsSplit.concat(m.split('\n'));
                    });

                    msgsSplit.forEach((m) => {
                        conn.send(`PRIVMSG ${target} :${m}`);
                    });
                },
                join: (conn, channel, whoDelay = 0) => {
                    if (whoDelay > 0) setTimeout(() => conn.send(`WHO ${channel}`), 1000 * whoDelay);
                    conn.send(`JOIN ${channel}`);
                },
                part: (conn, channel, reason = '') => conn.send(`PART ${channel} :${reason}`),
                identify: (conn, username, password) => conn.utility('message', 'NickServ', `IDENTIFY ${username} ${password}`),
                quit: (conn, reason = 'NIRCEX has been closed successfully.') => {
                    conn.send(`QUIT :${reason}`);
                    conn.close();
                },
            }
        };

        return data;
    }
};

nircex.Manager = class Manager {
    constructor(prefix, databaseFolderPath = null, messageTypes = []) {
        this.connections = {};
        this.handlers = [];
        this.commandNames = [];
        this.dbFolder = databaseFolderPath;
        this.config = null;
        this.configConnections = {};
        this.perms = new nircex.PermissionBase(path.join(this.dbFolder, 'permissions.json'));
        this.discordProxies = [];

        if (databaseFolderPath != null) {
            let dbp = path.join(databaseFolderPath, 'messages.db');
            this.msgBase = new nircex.MessageBase(dbp, messageTypes);
        }

        this.msgBase.on('message', (msg) => {
            let conn = this.connections[msg.connection];
            this.handlers.concat(conn.handlers).forEach((h) => {
                try {
                    h._handle(conn, msg);
                }

                catch (err) {
                    console.error(err);
                }
            });
        });

        this.commandPredicate = (name) => function (message) {
            return message.content.split(' ')[0] == prefix + name && message.type == 'privmsg';
        };
    }

    getHandlerNames() {
        return this.handlers.map((h) => h.name).filter((n) => n != null && n != '');
    }

    registerHandler(handler) {
        this.handlers.push(handler);
    }

    registerType(p) {
        this.msgBase.parsers.push(p);
    }

    registerTypes(ps) {
        this.msgBase.parsers = this.msgBase.parsers.concat(ps);
    }

    connect(name, host, port, configurers = {}, intervalOptions = {}, flags = []) {
        return this.connections[name] = new nircex.Connection(this, name, host, port, configurers, intervalOptions, flags);
    }

    loadPlugin(name, folder = './plugins') {
        const pfn = path.join(folder, name + '.js');

        if (!fs.existsSync(pfn))
            throw new Error(`${path.normalize(pfn)} does not exist or is not a file!`);

        delete require.cache[require.resolve(`./${pfn}`)];
        const plugin = require(`./${pfn}`)(nircex, this);

        (plugin.commands || []).forEach((cmd) => {
            if (this.commandNames.indexOf(cmd.name) > -1) {
                let nhandl = [];

                this.handlers.forEach((h) => {
                    if (h.name != cmd.name)
                        nhandl.push(h);

                    else
                        console.log('- Replacing command: ' + cmd.name);
                });

                this.handlers = nhandl;
            }

            else
                this.commandNames.push(cmd.name);

            this.registerHandler(new nircex.Handler(
                [this.commandPredicate(cmd.name)],
                (conn, msg) => {
                    msg.args = msg.content.split(' ').slice(1);

                    cmd.callback(conn, msg, (reply) => {
                        let p = this.perms.get(msg.properties.source);
                        let minP = (cmd.level != null ? cmd.level : 100);

                        if (p < minP)
                            conn.utility('message', (this.config != null && msg.properties.target == this.configConnections[conn.id].nick)
                                ? msg.properties.sourceNick
                                : msg.properties.target, `[Sorry, trust level ${minP}% required; your trust level is ${p}%.]`);

                        else
                            return conn.utility('message', (this.config != null && msg.properties.target == this.configConnections[conn.id].nick)
                                ? msg.properties.sourceNick
                                : msg.properties.target, reply);
                    });
                },
                cmd.name
            ));
        });

        (plugin.handlers || []).forEach((handler) => {
            this.registerHandler(new nircex.Handler(
                handler.predicates,
                handler.callback
            ));
        });

        (plugin.messageTypes || []).forEach((mtype) => {
            this.registerType(new nircex.MessageType(
                mtype.name,
                mtype.pattern,
                mtype.contentIndex || 0,
                mtype.propertyIndices || {},
                mtype.format || '',
                mtype.priority || 50
            ));
        });
    }

    loadConfig(data, pluginFolder = './plugins') {
        if ((typeof data) === 'string') data = JSON.parse(data);
        this.config = data;

        let resConns = [];
        let keys = {};

        if (data.plugins != null)
            data.plugins.forEach((p) => this.loadPlugin(p, pluginFolder));

        const readline = require('readline');

        const stdmut = new Writable({
            write: (chunk, encoding, callback) => {
                if (stdmut.open)
                    process.stdout.write(chunk, encoding);

                callback();
            }
        });

        stdmut.write('test\n');

        stdmut.open = true;

        const rl = readline.createInterface({
            input: process.stdin,
            output: stdmut,
            terminal: true
        });

        let questions = [];
        let callbacks = [];

        function getCert() {
            // wip
            return null;
        }

        function askNext() {
            let q = questions.shift();

            if (q != null)
                ask(q.q, q.c);

            else {    
                callbacks.forEach((cb) => {
                    cb[0](cb[1]);
                });
            }
        }

        function ask(question, callback) {
            stdmut.open = true;
            console.log();

            if (questions.length == 0) {
                rl.question(question, function (answer) {
                    stdmut.open = true;
                    console.log();
                    questions.shift();
                    callbacks.push([callback, answer]);
                    askNext();
                });
            }

            questions.push({
                q: question,
                c: callback
            });

            stdmut.open = false;
        }

        if (data.discordProxies != null) {
            data.discordProxies.forEach((p) => {
                ask(`Private key for the password named ${util.inspect(p.token)}, required to connect Discord proxy ${util.inspect(p.id)}: `, (tokenKey) => {
                    let proxy = new nircex.DiscordIRCProxy(cps.readPassword(p.token, tokenKey), p.port);
    
                    this.discordProxies.push(proxy);
                });
            });
        }

        data.connections.forEach((c) => {
            this.configConnections[c.name] = c;

            let configurer = nircex.configurers.irc(
                c.nickname || data.nickname || ('NIRCEXBot_' + randomID(15)),
                c.ident || data.ident || 'NIRCEX',
                c.realname || data.realname || 'A NIRCEX bot.',
                c.channels || [], c.joinDelay || 7
            );

            let conn;

            function auth(passwordKey) {
                conn.registerHandler(new nircex.OneShotHandler(
                    [nircex.predicates.irc.welcome],
                    (conn) => conn.utility('identify', c.account.username, cps.readPassword(c.account.password, passwordKey))
                ));
            }

            if (c.account != null) {
                if (Object.keys(keys).indexOf(c.account.password) > -1) {
                    setTimeout(() => {
                        conn = this.connect(c.name, c.hostname, c.port || 6667, [configurer]);
                    }, (c.preJoinDelay || 0) * 1000);
                    auth(keys[c.account.password]);
                }

                else {
                    let certAuth = getCert(c.account.password);

                    if (certAuth); // wip too

                    else
                        ask(`Private key for the password named ${util.inspect(c.account.password)}, required to connect to network ${util.inspect(c.name)}: `, (passwordKey) => {
                            conn = this.connect(c.name, c.hostname, c.port || 6667, [configurer]);

                            if (passwordKey == '')
                                console.warn(`WARNING: Ignoring account authentication for network ${c.name}.`);

                            else {
                                keys[c.account.password] = passwordKey;
                                auth(passwordKey);
                            }
                        });
                }
            }

            else
                callbacks.push([() => {
                    setTimeout(() => {
                        conn = this.connect(c.name, c.hostname, c.port || 6667, [configurer], c.flags || []);
                    }, (c.preJoinDelay || 0) * 1000);
                }, null]);

            resConns.push(conn);
        });

        return resConns;
    }
};


if (require.main !== module)
    module.exports = nircex;

else {
    if (!fs.existsSync('db'))
        fs.mkdirSync('db');

    let bot = new nircex.Manager('@@', 'db', nircex.ircTypes);
    bot.loadConfig(fs.readFileSync('nircex.json', 'utf-8'));
}